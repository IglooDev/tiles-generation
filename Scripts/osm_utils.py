import math

def num2deg(xtile, ytile, zoom):
  n = 2.0 ** zoom
  lon_deg = xtile / n * 360.0 - 180.0
  lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
  lat_deg = math.degrees(lat_rad)
  return (lat_deg, lon_deg)
  
def deg2num(lat_deg, lon_deg, zoom):
  lat_rad = math.radians(lat_deg)
  n = 2.0 ** zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
  return (xtile, ytile)

def getTileCoords(zoom, lat, lng):
  def degreeToRadian(degree):
    return degree * (math.pi / 180);
  def sec(x):
    return 1/math.cos(x)

  xtile = math.floor((lng + 180) / 360 * (1<<zoom))
  ytile = math.floor( (1 - math.log(math.tan(degreeToRadian(lat)) + 1 / math.cos(degreeToRadian(lat))) / math.pi) / 2 * (1<<zoom) )
  if xtile < 0:
      xtile=0
  if xtile >= (1<<zoom):
      xtile=((1<<zoom)-1)
  if ytile < 0:
      ytile=0;
  if ytile >= (1<<zoom):
      ytile=((1<<zoom)-1)
  return (xtile, ytile)


