import sqlite3
import os, shutil
osmUtils = __import__('osm_utils')


mapFolder = "examples"

if os.path.isdir(mapFolder + '/tiles'):
    shutil.rmtree(mapFolder + '/tiles');
os.mkdir(mapFolder + '/tiles');

conn = sqlite3.connect(mapFolder + "/jeuxduquebec2017.mbtiles") 
boundsCurs = conn.cursor();
boundsCurs.execute("SELECT value FROM metadata where name = 'bounds'")
boundsStr = boundsCurs.fetchone()[0]
boundsArray = boundsStr.split(",")
lng = float(boundsArray[0])
lat = float(boundsArray[3])


#print minTile
#exit();
curs = conn.cursor()
curs.execute("SELECT zoom_level, tile_column, tile_row, tile_id FROM MAP order by zoom_level, tile_column, tile_row desc")

cur_zoom_level = -1
cur_column = -1
x = 0
y = 0
base_level = 14
minTile = 0
for zoom_level, tile_column, tile_row, tile_id in curs:
    x = tile_column
    if cur_zoom_level != zoom_level:
        minTile = osmUtils.deg2num(lat, lng, zoom_level)
    if cur_column != tile_column:
        y = minTile[1]
        if zoom_level >= base_level:
            y = y + 1
    else:
        y = y + 1

    cur_column = tile_column
    cur_zoom_level = zoom_level
    
    tile_path = mapFolder + "/tiles/" + `zoom_level` + "/" + `x` + "/" + `y` + ".png"
    if not os.path.isdir(mapFolder + '/tiles/' + `zoom_level`):
      os.mkdir(mapFolder + '/tiles/' + `zoom_level`);
    if not os.path.isdir(mapFolder + '/tiles/' + `zoom_level` + "/" + `x`):
      os.mkdir(mapFolder + '/tiles/' + `zoom_level` + "/" + `x`);

    cursTile = conn.cursor();
    cursTile.execute("SELECT tile_data FROM images WHERE tile_id = '" + tile_id + "'")
    tile_data = cursTile.fetchone()[0]
    with open(tile_path, 'wb') as output_file:
        output_file.write(tile_data)

