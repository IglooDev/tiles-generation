from osgeo import osr, gdal
import os, shutil, json, zipfile, csv, base64, time
import sys, math, sqlite3, binascii
from PIL import Image, ImageOps

MAX_IMAGE_SIZE = 89478485;
SCRIPT_VERSION = "Python Script V 1.0"
OUTPUT_TILE_TYPE = "JPEG"
OUTPUT_TILE_EXT = "jpg"

def getBundleDefinition(nameFr, nameEn, uniqueId, htmlDescriptionFr, htmlDescriptionEn, centerPoint, iconName, version, keywords, accessCodes):
  bundleDef = {
    "name": 
    {
      "fr": nameFr,
      "en": nameEn
    },
    "version": 0.1,
    "formatVersion": 2.00,
    "uniqueIdentifier": uniqueId,
    "htmlDescription": 
    {
      "fr": htmlDescriptionFr,
      "en": htmlDescriptionEn
    },
    "centerPoint": {
      "latLong": [
        48.33562374728355, -71.58362533637917
      ],
      "diameter": 10
    },
    "iconImageUrl":
    {
      "2x": "$FILE_STORAGE_REMOTE_URL/2.00/_icons/" + iconName + "@2x.png?v=1",
      "1x": "$FILE_STORAGE_REMOTE_URL/2.00/_icons/" + iconName + "@1x.png?v=1"
    }
  }
  if (accessCodes != 0):
    bundleDef["accessCodes"] = accessCodes
  if (version > 0):
    bundleDef["version"] = version
  lat, lng = centerPoint
  bundleDef["centerPoint"]["latLong"] = [lat, lng]
  if (keywords != 0 and keywords != ''):
    bundleDef["keywords"] = keywords
  return bundleDef

def saveTile(tile, filename, filetype):
    if filetype == "JPEG":
      tile.convert('RGB').save(filename, filetype, quality=80)
    else:
      tile.save(filename, filetype)

def smartResizeImage(source, destinationSize, resample, mode):
    result = 0
    w, h = source.size
    dw, dh = destinationSize
    if w*h > MAX_IMAGE_SIZE:
        dim = destinationSize
        if (dw*dh <= MAX_IMAGE_SIZE):
            # get max size supported
            scale = float((MAX_IMAGE_SIZE**(1/2.0))) / float(((w*h)**(1/2.0)));
            if (scale < 1):
                dw = int(math.ceil(w*scale));
                dsch = dw/float(w)
                dh = int(MAX_IMAGE_SIZE/float(dw))
                dim = (dw, dh)
        ratio = float(dw)/float(w)
        result = Image.new(mode, dim)
        tilesize = 2560
        columns = w/tilesize
        rows = h/tilesize
        for x in  range(0,columns+1):
          for y in  range(0,rows+1): 
            if (x*tilesize<w and y*tilesize<h):
              minx = max(0, x*tilesize-tilesize/2)
              miny = max(0, y*tilesize-tilesize/2)
              maxx = min(tilesize+x*tilesize+tilesize/2,w)
              maxy = min(tilesize+y*tilesize+tilesize/2, h)
              tw = maxx-minx
              th = maxy-miny
              rt = source.crop((minx, miny, maxx, maxy)).resize((int(round(tw*ratio)),int(round(th*ratio))), resample)
              cx = 0 if minx == 0 else int(round(tilesize*ratio))/2
              cy = 0 if miny == 0 else int(round(tilesize*ratio))/2
              cx2, cy2 = rt.size
              if maxx != w:
                cx2 = cx + int(round(tilesize*ratio))
              if maxy != h:
                cy2 = cy + int(round(tilesize*ratio))
              offset = (int(round(tilesize*ratio*x)), int(round(tilesize*ratio*y)))
              result.paste(rt.crop((cx, cy, cx2, cy2)),offset);
        if (dim != destinationSize):
          result = smartResizeImage(result, destinationSize, resample, mode);
    else:
        result = source.resize(destinationSize, resample)
    return result


def importGeotiffMap(filePath, mapName, mapFolderName, logoPath, outputBasePath, mapFolderPrefix):
    exportInfo = {"centerPoint": 0}
    start = time.time()
    mapGeoTiffPath = filePath
    mapFolder = outputBasePath + "/" + mapFolderPrefix + mapFolderName
    if os.path.isdir(mapFolder):
      shutil.rmtree(mapFolder);
    os.mkdir(mapFolder);
    os.mkdir(mapFolder + '/WebViewer');
    os.mkdir(mapFolder + '/OMB');

    print("Importing " + mapName + " in " + mapFolder)

    itemsJson = [
        {
            "mapDefinitionFilename": "MapDefinition.json",
            "name": {
                "en": mapName,
                "fr": mapName
            },
            "mapBanner": {
                "2x": "data:image/png;base64,",
                "1x": "data:image/png;base64,"
            },
            "uid": "1"
        }
    ]

    tilesize = 256
    mapDefinition = {
        "modelVersion": 2,
        "maxZoom": 7,
        "initialZoom": 1,
        "styles": [],
        "uid": "1",
        "objects": [],
        "mapBuilderVersion": SCRIPT_VERSION,
        "name": {
            "en": mapName,
            "fr": mapName
        },
        "projection": {
            "tiles": [
                {
                    "tileType": "ONDAGO",
                    "minZoom": 0,
                    "database": "tilesets/tiles.db",
                    "folder": "tileset/",
                    "imageFormat": OUTPUT_TILE_EXT,
                    "tileSize": tilesize,
                    "maxZoom": 0,
                    "offset": {
                        "x": 0,
                        "y": 0
                    },
                    "baseZoom": 0
                }
            ],
            "referencePoints": [
                
            ],
            "type": "artistic",
            "mapDimension": {
                "width": 0,
                "height": 0
            }
        },
        "contentInsets": {
            "right": 0,
            "top": 60,
            "bottom": 40,
            "left": 0
        },
        "sharePosition": "on",
        "type": "map",
        "initiallyCenteredOnUserPosition": True}

    ds = gdal.Open(mapGeoTiffPath)
    old_cs= osr.SpatialReference()
    #print ds.GetProjectionRef()
    #exit()
    old_cs.ImportFromWkt(ds.GetProjectionRef())

    # create the new coordinate system
    wgs84_wkt = """
    GEOGCS["WGS 84",
        DATUM["WGS_1984",
            SPHEROID["WGS 84",6378137,298.257223563,
                AUTHORITY["EPSG","7030"]],
            AUTHORITY["EPSG","6326"]],
        PRIMEM["Greenwich",0,
            AUTHORITY["EPSG","8901"]],
        UNIT["degree",0.01745329251994328,
            AUTHORITY["EPSG","9122"]],
        AUTHORITY["EPSG","4326"]]"""
    new_cs = osr.SpatialReference()
    new_cs .ImportFromWkt(wgs84_wkt)

    # create a transform object to convert between coordinate systems
    transform = osr.CoordinateTransformation(old_cs,new_cs) 


    # GDAL affine transform parameters, According to gdal documentation xoff/yoff are image left corner, a/e are pixel wight/height and b/d is rotation and is zero if image is north up. 
    xoff, a, b, yoff, d, e = ds.GetGeoTransform()

    def pixel2coord(x, y):
      """Returns global coordinates from pixel x, y coords"""
      xp = a * x + b * y + xoff
      yp = d * x + e * y + yoff
      return(xp, yp)

    def getNumZoom(size):
      result = 1;
      imSize = size;
      while imSize > tilesize*2:
        result = result + 1;
        imSize = imSize/2;
      return result;
    def escapeQuote(string):
      return string.replace("'", "''")
    def zipdir(path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file in files:
                ziph.write(os.path.join(root, file))



    def makeRefPoint(x, y, lat, lng):
      refPoint = {"location": {
                  "latitude": lat,
                  "longitude": lng
              },
              "coordinate": {
                  "x": x,
                  "y": y
              }
          }
      return refPoint

    def pointToLocation(x, y, transform):
      coords = pixel2coord(x,y)
      latlong = transform.TransformPoint(coords[0],coords[1])
      return (latlong[1], latlong[0])

    def addRefPointAt(x, y, transform, points):
      coords = pixel2coord(x,y)
      latlong = transform.TransformPoint(coords[0],coords[1])
      refPoint = makeRefPoint(x, y, latlong[1], latlong[0])
      points.append(refPoint)

                
    # map size
    map_height = ds.RasterYSize;
    map_width = ds.RasterXSize;


    #Properly close the datasets to flush to disk
    #dst_ds = None
    ds = None

    #prepare database
    conn = sqlite3.connect("tiles.db") 
    curs = conn.cursor()
    curs.execute("CREATE TABLE IF NOT EXISTS metadata (name TEXT, value TEXT);").fetchone()
    curs.execute("CREATE TABLE IF NOT EXISTS tiles (zoom_level INTEGER, tile_column INTEGER, tile_row INTEGER, tile_data NONE);").fetchone()

    # name
    # bounds
    # format
    # version
    # description
    # type
    curs.execute("INSERT INTO metadata (name, value) VALUES ('format', '{:s}');".format(OUTPUT_TILE_EXT))
    curs.execute("INSERT INTO metadata (name, value) VALUES ('version', '1');")
    curs.execute("INSERT INTO metadata (name, value) VALUES ('type', 'baselayer');")
    mapImage = Image.open(mapGeoTiffPath);
    ratio = 1
    w, h = mapImage.size

    mapDefinition["projection"]["mapDimension"]["width"] = w
    mapDefinition["projection"]["mapDimension"]["height"] = h

    # 560 x 400 image for map banner 2x
    bannerW = 560
    bannerH = 400
    thumbImage = Image.new("RGBA", (bannerW,bannerH))
    rsize = ((400*w/h),400) if float(bannerW/bannerH) > float(w/h) else  (560, (560*h/w))
    thumb = ImageOps.fit(smartResizeImage(mapImage, rsize, Image.ANTIALIAS, "RGBA"),(bannerW,bannerH),Image.ANTIALIAS)
    thumbImage.paste(thumb)
    if os.path.isfile(logoPath):
        logoImage = Image.open(logoPath)
        logoW,logoH = logoImage.size
        offset = (bannerW - logoW - 10, bannerH - logoH - 10)
        thumbImage.paste(logoImage, offset, logoImage)
    thumbImage.save(mapFolder + "/MapBanner@2x.png", "PNG")
    with open(mapFolder + "/MapBanner@2x.png") as thumbImageFile:
        thumbImage2x = base64.b64encode(thumbImageFile.read())
        itemsJson[0]["mapBanner"]["2x"] = "data:image/png;base64," + thumbImage2x

    thumbImage = thumbImage.resize((bannerW/2,bannerH/2), Image.ANTIALIAS)
    thumbImage.save(mapFolder + "/MapBanner.png", "PNG")
    with open(mapFolder + "/MapBanner.png") as thumbImageFile:
        thumbImage1x = base64.b64encode(thumbImageFile.read())
        itemsJson[0]["mapBanner"]["1x"] = "data:image/png;base64," + thumbImage1x

    exportInfo["centerPoint"] = pointToLocation(w/2, h/2, transform)
    # build reference points
    if __name__ == "__main__":
    # get columns and rows of your image from gdalinfo
     max_points = 20
     refPointDistanceX = w/max_points
     refPointDistanceY = h/max_points
     pointRows = (map_height/refPointDistanceY)+2;
     pointCols = (map_width/refPointDistanceX)+2;
     referencePoints = mapDefinition["projection"]["referencePoints"]
     for row in  range(1,pointRows):
      for col in  range(1,pointCols): 
        if row % 2 != col % 2:
          addRefPointAt(col*refPointDistanceX,row*refPointDistanceY, transform, referencePoints)

     addRefPointAt(0, 0, transform, referencePoints)
     addRefPointAt(w, h, transform, referencePoints)
     addRefPointAt(0, h, transform, referencePoints)
     addRefPointAt(w, 0, transform, referencePoints)

    minSize = min(h, w);
    numZoom = getNumZoom(minSize);
    tilesImage = mapImage;
    os.mkdir(mapFolder + '/WebViewer/tileset');
    os.mkdir(mapFolder + '/OMB/tilesets');
    mapDefinition["projection"]["tiles"][0]["maxZoom"] = numZoom
    mapDefinition["projection"]["tiles"][0]["baseZoom"] = numZoom
    for zoom in range (numZoom,1, -1):
      mapDefinition["projection"]["tiles"][0]["minZoom"] = zoom
      zoom_dir = mapFolder + "/WebViewer/tileset/{:d}/".format(zoom)    
      os.mkdir(zoom_dir)        
      if (zoom != numZoom):
        ratio = ratio * 2;
        tilesImage = smartResizeImage(mapImage, (w/ratio, h/ratio), Image.ANTIALIAS, "RGBA")
      imageW, imageH = tilesImage.size
      xNumTiles = int(math.ceil(imageW/tilesize))+1
      yNumTiles = int(math.ceil(imageH/tilesize))+1

      for x in  range(0,xNumTiles):
        x_dir = zoom_dir + "{:d}/".format(x)
        os.mkdir(x_dir)
        for y in  range(0,yNumTiles): 
          if (x*tilesize<imageW and y*tilesize<imageH):
              tile = 0;
              if (tilesize+x*tilesize<imageW and tilesize+y*tilesize<imageH):
                tile = tilesImage.crop((x*tilesize, y*tilesize, tilesize+x*tilesize, tilesize+y*tilesize))
              else:
                tile = Image.new('RGBA', (tilesize,tilesize))
                cropped = tilesImage.crop((x*tilesize, y*tilesize, min(tilesize+x*tilesize,imageW), min(tilesize+y*tilesize, imageH)));
                tile.paste(cropped, (0,0))
              saveTile(tile, x_dir + "{:d}.{:s}".format(y, OUTPUT_TILE_EXT), OUTPUT_TILE_TYPE)
              tilestring = ""
              with open(x_dir + "{:d}.{:s}".format(y, OUTPUT_TILE_EXT), 'rb') as f:
                content = f.read()
                tilestring = "X'" + binascii.hexlify(content) + "'"
                curs.execute("INSERT INTO tiles (zoom_level, tile_column, tile_row, tile_data) VALUES (?, ?, ?, ?)", [ zoom, x, y, buffer(content)])
    #create tiles index
    curs.execute("CREATE INDEX row_column_zoom ON tiles (tile_row ASC, tile_column ASC, zoom_level ASC);")
    # Close database
    curs.close()
    conn.commit()
    conn.close()
    #dump map definition & items
    with open(mapFolder + '/OMB/MapDefinition.json', 'w') as outfile:
        json.dump(mapDefinition, outfile, indent=4, sort_keys=True, separators=(',', ':'))
    with open(mapFolder + '/WebViewer/MapDefinition.json', 'w') as outfile:
        json.dump(mapDefinition, outfile, indent=4, sort_keys=True, separators=(',', ':'))
    with open(mapFolder + '/OMB/items.json', 'w') as outfile:
        json.dump(itemsJson, outfile, indent=4, sort_keys=True, separators=(',', ':'))
    with open(mapFolder + '/items.json', 'w') as outfile:
        json.dump(itemsJson, outfile, indent=4, sort_keys=True, separators=(',', ':'))
    #move db file
    shutil.move("tiles.db", mapFolder + "/OMB/tilesets/tiles.db")
    # Zip content 
    if __name__ == '__main__':
        zipf = zipfile.ZipFile(mapFolder + '/content.omb', 'w', zipfile.ZIP_DEFLATED)
        #zipdir(mapFolder + '/OMB/tilesets', zipf)
        zipf.write(mapFolder + '/OMB/MapDefinition.json', 'MapDefinition.json')
        zipf.write(mapFolder + '/OMB/tilesets/tiles.db', 'tilesets/tiles.db')
        zipf.close()
    if os.path.isdir(mapFolder + '/OMB'):
      shutil.rmtree(mapFolder + '/OMB');
    end = time.time()
    print "imported in {:d} seconds".format(int(round((end - start))))
    return exportInfo
baseFolder = "examples/"
csvfile = baseFolder + "batchUpload.csv"
baseExportPath = baseFolder + "export"
mapFolderPrefix = "GPSQuebec_"
uniqueIdPrefix = mapFolderPrefix
sectionJson = {
      "uniqueIdentifier": "gpsMaps",
      "order": 10,
      "bundleRendererType": "icon",
      "name":
      {
        "en": "GPS Maps",
        "fr": "Cartes GPS"
      },
      "bundles": []
    }



if os.path.isfile(csvfile):
  bundles = []
  with open(csvfile) as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';')
    for row in reader:
      filename = baseFolder + row['file']
      if os.path.isfile(filename):
        uniqueId = row['title'].replace (" ", "_")
        exportInfo = importGeotiffMap(filename, row['title'], uniqueId, baseFolder + "logoGpsQuebec.png", baseExportPath, mapFolderPrefix)
        description = '<p style="font-family: sans-serif; text-align: left;">{:s}</p>'.format(row['description'])
        name = row['title']
        iconName = "GPSQuebec"
        accessCodes = ["ondago123", "gpsQuebec2017"]
        bundleDefinition = getBundleDefinition(name, name, mapFolderPrefix + uniqueId, description, description, exportInfo["centerPoint"], iconName, 0.1, row['search_tags'], accessCodes)
        bundles.append(bundleDefinition)
        sectionJson["bundles"].append(mapFolderPrefix + uniqueId)

  with open(baseExportPath + '/bundles.json', 'w') as outfile:
        json.dump(bundles, outfile, indent=4, sort_keys=True, separators=(',', ':'))
  with open(baseExportPath + '/sections.json', 'w') as outfile:
        json.dump(sectionJson, outfile, indent=4, sort_keys=True, separators=(',', ':'))

  
      #print row['file'], row['title'], row['description'], row['published'], row['publisher'], row['search_tags'], row['category_1'], row['category_2'], row['category_3'], row['language_1'], row['language_2'], row['country_1'], row['apple_sku']

#  filename = 'QC_021L15.tif';
#  if os.path.isfile(filename):
#    importGeotiffMap(filename, '021L15 SAINT-RAPHAEL', "QC_021L15", '')
