from PIL import Image, ImageOps
import math
MAX_IMAGE_SIZE = 89478485;

def saveTile(tile, filename, filetype):
    if filetype == "JPEG":
      tile.convert('RGB').save(filename, filetype, quality=80)
    else:
      tile.save(filename, filetype)

def smartResizeImage(source, destinationSize, resample, mode):
    result = 0
    w, h = source.size
    dw, dh = destinationSize
    if w*h > MAX_IMAGE_SIZE:
        dim = destinationSize
        if (dw*dh <= MAX_IMAGE_SIZE):
            # get max size supported
            scale = float((MAX_IMAGE_SIZE**(1/2.0))) / float(((w*h)**(1/2.0)));
            if (scale < 1):
                dw = int(math.ceil(w*scale));
                dsch = dw/float(w)
                dh = int(MAX_IMAGE_SIZE/float(dw))
                dim = (dw, dh)
        ratio = float(dw)/float(w)
        result = Image.new(mode, dim)
        tilesize = 2560
        columns = w/tilesize
        rows = h/tilesize
        for x in  range(0,columns+1):
          for y in  range(0,rows+1): 
            if (x*tilesize<w and y*tilesize<h):
              minx = max(0, x*tilesize-tilesize/2)
              miny = max(0, y*tilesize-tilesize/2)
              maxx = min(tilesize+x*tilesize+tilesize/2,w)
              maxy = min(tilesize+y*tilesize+tilesize/2, h)
              tw = maxx-minx
              th = maxy-miny
              rt = source.crop((minx, miny, maxx, maxy)).resize((int(round(tw*ratio)),int(round(th*ratio))), resample)
              cx = 0 if minx == 0 else int(round(tilesize*ratio))/2
              cy = 0 if miny == 0 else int(round(tilesize*ratio))/2
              cx2, cy2 = rt.size
              if maxx != w:
                cx2 = cx + int(round(tilesize*ratio))
              if maxy != h:
                cy2 = cy + int(round(tilesize*ratio))
              offset = (int(round(tilesize*ratio*x)), int(round(tilesize*ratio*y)))
              result.paste(rt.crop((cx, cy, cx2, cy2)),offset);
        if (dim != destinationSize):
          result = smartResizeImage(result, destinationSize, resample, mode);
    else:
        result = source.resize(destinationSize, resample)
    return result


