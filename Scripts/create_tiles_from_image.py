from PIL import Image, ImageOps
import os
import math
OUTPUT_TILE_TYPE = "JPEG"
OUTPUT_TILE_EXT = "jpg"

imageUtils = __import__('image_utils')
tilesize = 256
def getNumZoom(size):
      result = 1;
      imSize = size;
      while imSize > tilesize*2:
        result = result + 1;
        imSize = imSize/2;
      return result;

mapFolder = "examples"

mapImage = Image.open(mapFolder + "/map.png")
ratio = 1
w, h = mapImage.size
minSize = min(h, w);
numZoom = getNumZoom(minSize);
tilesImage = mapImage;
os.mkdir(mapFolder + '/tileset');
for zoom in range (numZoom,1, -1):
      zoom_dir = mapFolder + "/tileset/{:d}/".format(zoom)    
      os.mkdir(zoom_dir)        
      if (zoom != numZoom):
        ratio = ratio * 2;
        tilesImage = imageUtils.smartResizeImage(mapImage, (w/ratio, h/ratio), Image.ANTIALIAS, "RGBA")
      imageW, imageH = tilesImage.size
      xNumTiles = int(math.ceil(imageW/tilesize))+1
      yNumTiles = int(math.ceil(imageH/tilesize))+1

      for x in  range(0,xNumTiles):
        x_dir = zoom_dir + "{:d}/".format(x)
        os.mkdir(x_dir)
        for y in  range(0,yNumTiles): 
          if (x*tilesize<imageW and y*tilesize<imageH):
              tile = 0;
              if (tilesize+x*tilesize<imageW and tilesize+y*tilesize<imageH):
                tile = tilesImage.crop((x*tilesize, y*tilesize, tilesize+x*tilesize, tilesize+y*tilesize))
              else:
                tile = Image.new('RGBA', (tilesize,tilesize))
                cropped = tilesImage.crop((x*tilesize, y*tilesize, min(tilesize+x*tilesize,imageW), min(tilesize+y*tilesize, imageH)));
                tile.paste(cropped, (0,0))
              imageUtils.saveTile(tile, x_dir + "{:d}.{:s}".format(y, OUTPUT_TILE_EXT), OUTPUT_TILE_TYPE)