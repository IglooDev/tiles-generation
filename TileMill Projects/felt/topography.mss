@topographic-10: darken(@land-color, 45%);
@topographic-50: darken(@land-color, 50%);
@topographic-100: darken(@land-color, 55%);
@topographic-text-10: darken(@land-color, 25%);
@topographic-text-50: darken(@land-color, 30%);
@topographic-text-100: darken(@land-color, 35%);
@topographic-text-halo: lighten(@land-color, 30%);

//:end of consts
#topography_10[zoom>14] {
      line-width: 0.5;
      line-color: @topographic-10;
      line-opacity: 0.6;
} 

#topography_50[zoom>11] {
      line-width: 0.5;
      line-color: @topographic-50; 
      line-opacity: 0.6;
      [zoom>14] {
        text-name: "[height]";
        text-opacity:0.9;
        text-size: 9;
        text-fill: @topographic-text-50;
        text-face-name: @book-fonts;
        text-halo-radius: 2;
        text-halo-fill: @topographic-text-halo;
        text-wrap-width: 50;
        text-placement: line;
      }
}

#topography_100[zoom>8] {
      line-width: 0.5;
      line-color: @topographic-100;
      line-opacity: 0.4;
      [zoom>9] {
        line-width: 0.5;
        line-color: @topographic-100;
        line-opacity: 0.6;
      }
      [zoom>13] {
        text-name: "[height]";
        text-opacity:0.9;
        text-size: 9;
        text-fill: @topographic-text-100;
        text-face-name: @book-fonts;
        text-halo-radius: 2;
        text-halo-fill: @topographic-text-halo;
        text-wrap-width: 50;
        text-placement: line;
      }
  
}