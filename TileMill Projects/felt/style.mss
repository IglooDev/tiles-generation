Map {
  background-color: @water-color;
}

@book-fonts:    "DejaVu Sans Book";
@bold-fonts:    "DejaVu Sans Bold", "DejaVu Sans Book";

@oblique-fonts: "DejaVu Sans Oblique", "DejaVu Sans Book";

@water-color: #719399; 
@land-color: #f3f0e1;  
@text-fill: #466277;
@text-halo-fill: rgba(255,255,239,0.2);
@text-halo-fill-plain: #ffffef;
@tree-color: #c1dfb2;
@tree-text: #81ae80;
@water-name: #f2efe0;
  
//:end of consts
#texture[zoom>1] {
  polygon-pattern-file:url(textures/ultralight-felt.png);
}
