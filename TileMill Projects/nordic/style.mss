Map {
  background-color: @water-color;
}

@book-fonts:    "DejaVu Sans Book";
@bold-fonts:    "DejaVu Sans Bold", "DejaVu Sans Book";

@oblique-fonts: "DejaVu Sans Oblique", "DejaVu Sans Book";

@water-color: #a4cbed; 
@land-color: #cbe0e8;  
@text-fill: #000000;
@text-halo-fill: rgba(255,255,239,0.2);
@text-halo-fill-plain: #ffffef;
@tree-color: #91c0cf;
@tree-text: #000000;
@water-name: #000000;

