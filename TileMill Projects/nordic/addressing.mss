@addressing-color: @land-color;
@addressing-text: @text-fill;
@building-text: mix(@text-fill, @addressing-text, 50%);

//:end of consts

#interpolation {
  [zoom >= 17] {
    line-color: @addressing-color;
    line-width: 1;
    line-dasharray: 2,4;
  }
}

#housenumbers {
  [zoom >= 17] {
    text-name: "[addr:housenumber]";
    text-placement: interior;
    text-min-distance: 1;
    text-wrap-width: 0;
    text-face-name: @book-fonts;
    text-fill: @addressing-text;
    text-size: 9;
  }
}

#housenames {
  [zoom >= 17] {
    text-name: "[addr:housename]";
    text-placement: interior;
    text-wrap-width: 20;
    text-face-name: @book-fonts;
    text-size: 8;
    text-fill: @addressing-text;
    [zoom >= 18] {
      text-size: 9;
    }
  }
}

#building-text {
  [zoom >= 14][way_pixels > 3000],
  [zoom >= 17] {
    text-name: "[name]";
    text-size: 11;
    text-fill: @building-text;
    text-face-name: @book-fonts;
    text-halo-radius: 1;
    text-wrap-width: 20;
    text-halo-fill: @text-halo-fill;
    text-placement: interior;
  }
}
