Map {
  background-color: @water-color;
}
@ondago-blue: #008eab;
@ondago-green: #148637;
@ondago-baby-blue: #bae3e3;
@ondago-light-green: #67ad2f;
@ondago-orange: #ed6e25;
@ondago-ground: #acd5de;

@book-fonts:    "DejaVu Sans Book";
@bold-fonts:    "DejaVu Sans Bold", "DejaVu Sans Book";

@oblique-fonts: "DejaVu Sans Oblique", "DejaVu Sans Book";

@water-color: #afd3dc; 
@land-color: lighten(@ondago-ground, 20%); 
@building-base-color: #d4d3c9;
  
@text-fill: #466277;
@text-halo-fill: rgba(255,255,239,0.2);
@text-halo-fill-plain: #ffffef;

@railway-color: #aaa;
@road-color: #f6f6f6;
@tree-color: #accf69;
@tree-text: #81ae80;
@water-name: @ondago-blue;

//:end of consts
#texture[zoom>1] {
  polygon-pattern-file:url(textures/low-contrast-linen.png);
  polygon-comp-op:overlay;
  polygon-pattern-opacity: 0.3;
}