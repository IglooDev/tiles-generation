Map {
  background-color: @water-color;
}

@book-fonts:    "DejaVu Sans Book";
@bold-fonts:    "DejaVu Sans Bold", "DejaVu Sans Book";

@oblique-fonts: "DejaVu Sans Oblique", "DejaVu Sans Book";

@water-color: #021019; 
@land-color: #08314a;  
@text-fill: #fff;
@text-halo-fill: rgba(0,0,0,0.1);
@text-halo-fill-plain: #000;

//:end of consts
