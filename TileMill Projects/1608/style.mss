Map {
  background-color: @water-color;
}

@book-fonts:    "DejaVu Sans Book";
@bold-fonts:    "DejaVu Sans Bold", "DejaVu Sans Book";

@oblique-fonts: "DejaVu Sans Oblique", "DejaVu Sans Book";

@water-color: #b0cdbe; 
@land-color: #f2cb85;  
@text-fill: #6f4c0b;
@text-halo-fill: rgba(238,218,165,0.1);
@text-halo-fill-plain: #eedaa5;

//:end of consts
#paper[zoom>1] { 
  polygon-pattern-file:url(textures/old-map.png);
}
/*
#topography_10[zoom>14] {
      line-width: 0.5;
      line-color: #000;
      line-opacity: 0.1;
  polygon-fill:transparent;
}
#topography_50[zoom>11] {
      line-width: 0.5;
      line-color: #000; 
      line-opacity: 0.3;
  polygon-fill:transparent;
}
#topography_100 {
      line-width: 0.5;
      line-color: #000;
      line-opacity: 0.5;
  
      polygon-fill:transparent;
  
    text-name: "[height]";
    text-size: 9;
    text-fill: #000;
    text-face-name: @book-fonts;
    text-halo-radius: 1.5;
    text-wrap-width: 50;
    text-placement: line;
    [zoom >= 4] {
      text-size: 10;
    }
  
}
#topography_100d {
      line-width: 1;
      line-color: #00f;
      line-opacity: 1;
  
      polygon-fill:transparent;
  
    text-name: "[height]";
    text-size: 9;
    text-fill: #000;
    text-face-name: @book-fonts;
    text-halo-radius: 1.5;
    text-wrap-width: 50;
    text-placement: line;
    [zoom >= 4] {
      text-size: 10;
    }
  
}
#topography_100c {
      line-width: 1;
      line-color: #0f0;
      line-opacity: 1;
  
      polygon-fill:transparent;
  
    text-name: "[height]";
    text-size: 9;
    text-fill: #000;
    text-face-name: @book-fonts;
    text-halo-radius: 1.5;
    text-wrap-width: 50;
    text-placement: line;
    [zoom >= 4] {
      text-size: 10;
    }
  
}
#topography_100b {
      line-width: 1;
      line-color: #f00;
      line-opacity: 1;
  
      polygon-fill:transparent;
  
    text-name: "[height]";
    text-size: 9;
    text-fill: #000;
    text-face-name: @book-fonts;
    text-halo-radius: 1.5;
    text-wrap-width: 50;
    text-placement: line;
    [zoom >= 4] {
      text-size: 10;
    }
  
}*/