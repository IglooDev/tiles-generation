Map {
  background-color: @water-color;
}

@book-fonts:    "DejaVu Sans Book";
@bold-fonts:    "DejaVu Sans Bold", "DejaVu Sans Book";

@oblique-fonts: "DejaVu Sans Oblique", "DejaVu Sans Book";

@water-color: #88b3fc; 
@land-color: #f0ede5;  
@text-fill: #000000;
@text-halo-fill: rgba(255,255,255,0.1);
@text-halo-fill-plain: white;

//:end of consts
